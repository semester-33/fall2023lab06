package backend;
import java.util.Random;

public class RpsGame {
    private int wins;
    private int ties;
    private int losses;
    private String message;
    private final Random rand = new Random();

    public RpsGame(){
        this.wins = 0;
        this.ties = 0;
        this.losses = 0;
    }

    public int getWins(){
        return this.wins;
    }

    public int getTies(){
        return this.ties;
    }

    public int getLosses(){
        return this.losses;
    }

    public String getMessage(){
        return this.message;
    }

    public void playRound(String playerChoice){
        int index = rand.nextInt(3);
        String[] actions = {"rock", "paper", "scissors"};

        if(actions[index].equals(playerChoice)){
            this.message = "Computer played " + actions[index] + " and no one won, it was a tie!";
            this.ties++;
        }
        else if((actions[index].equals("rock") && playerChoice.equals("scissors")) || (actions[index].equals("scissors") && playerChoice.equals("paper")) || (actions[index].equals("paper") && playerChoice.equals("rock"))){
            this.message = "Computer plays " + actions[index] + " and computer won!";
            this.losses++;
        }
        else if((actions[index].equals("scissors") && playerChoice.equals("rock")) || (actions[index].equals("paper") && playerChoice.equals("scissors")) || (actions[index].equals("rock") && playerChoice.equals("paper"))){
            this.message = "Computer plays " + actions[index] + " and computer lost!";
            this.wins++;
        }
    }
}
