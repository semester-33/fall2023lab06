package rpsgame;

import backend.RpsGame;
import java.util.Scanner;

/**
 * Console application for rock paper scissors game
 *
 */
public class App 
{
    public static void main( String[] args )
    {
        Scanner reader = new Scanner(System.in);
        RpsGame rps = new RpsGame();       
        boolean startGame = true;
        
        while(startGame){
            System.out.println("Press quit if you want to leave, otherwise please choose an action - rock - - paper - - scissors -");    
            String player = reader.next();
        
            if(player.equalsIgnoreCase("quit")){
                System.out.println("player has quit! now computer is sad");
                startGame = false;
            }

            rps.playRound(player);
            System.out.println(rps.getMessage());
        }

        reader.close();
    }
}
